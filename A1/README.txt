Please provide instructions here for how to build and run your project.

The preferred build system is make.

If you do not know make, any other single command that will build and run your project is acceptable.

--------

The file to be run is "proj_1.py" in the src directory
Enter on the command line: python3 proj_1.py
